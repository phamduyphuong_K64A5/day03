<?php
$products = array(
    "KDL" => "khoa học vật liệu",
    "MAT" => "Khoa học máy tính"
);
$gioitinh = array(
    0 => "Nam",
    1 => "Nữ"
); ?>

<html>

<head>
    <title>Đăng nhập</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <div class="mx-auto border d-flex flex-column" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width: 600px;border: 2px solid #2980D7!important;">

        <form class="my-3 d-flex flex-column mx-auto">
            <div class="d-flex mb-2" style="height: 30px">
                <label class="h-100 mr-1" style="width: 120px;border: 1px solid blue;color: white;background: #007bff;padding :5px" for="uname">Họ và tên</label>
                <input class="h-100" type="text" style="border: 1px solid blue" name="uname" required>
            </div>
            <div class="d-flex mb-2" style="height: 30px">
                <label class="h-100 mr-3" style="width: 100px;border: 1px solid blue;color: white;background: #007bff;padding :5px" for="uname">Giới tính</label>
                <div class="d-flex align-items-center">
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <div class="mr-3">
                            <input type="radio" class="mr-1" name=$i value=><?php echo $gioitinh[$i] ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="d-flex" style="height: 30px">
                <label class="h-100 mr-1" style="width: 100px;border: 1px solid blue;color: white;background: #007bff;padding :5px" for="uname">Phân khoa</label>
                <select style="width: 189px;border: 1px solid blue">
                    <?php foreach ($products as $product) : ?>
                        <option><?php echo $product ?></option>
                    <?php endforeach; ?>
                </select>
                <br><br>
            </div>

            <button type="submit" style="width: 120px;background-color: #70ad47!important; border-color: #70ad47" class="mx-auto btn btn-primary mt-3">Đăng ký</button>
        </form>
    </div>
</body>

</html>